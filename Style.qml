pragma Singleton
import QtQuick 2.0

QtObject {
    property int textSize: 14
    property color textColor: "#fac637"
    property color backgroundColor: "#03041a"
    property color controlsColor: "#3242a8"
}

