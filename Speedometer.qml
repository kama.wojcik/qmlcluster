import QtQuick 2.14

Gauge {
    currentAngle: valueProvider.speedValue
    currentSpeedValue: valueProvider.speedValue
    isMirrored: true
    unit: dataObject.speedUnit
}
