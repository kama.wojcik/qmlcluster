#include "datamodel.h"

#include <settingsmanager.h>

DataObject::DataObject(QObject *parent) : QObject(parent)
{
    SettingsManager settingManager;
    m_speedUnit = settingManager.getSpeedUnit();
}

QString DataObject::speedUnit() const
{
    return m_speedUnit;
}
