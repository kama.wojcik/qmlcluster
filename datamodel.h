#ifndef DATAMODEL_H
#define DATAMODEL_H

#include <QObject>
#include <QString>

class DataObject : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString speedUnit MEMBER m_speedUnit)

public:
    explicit DataObject(QObject *parent = nullptr);
    QString speedUnit() const;

private:
    QString m_speedUnit;
};

#endif // DATAMODEL_H
