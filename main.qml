import QtQuick 2.14
import QtQuick.Window 2.14
import QtQuick.Layouts 1.14
import QtQuick.Controls 2.14

Window {
    id: mainWindow
    visible: true
    width: 1600
    height: 480
    color: Style.backgroundColor
    title: qsTr("CLUSTER DEMO v1.0")

    RowLayout {
        anchors.fill: parent
        spacing: 6
        Rectangle {
            width: mainWindow.width / 3
            height: mainWindow.height
            Tachometer { id: tachometer }
        }
        Rectangle {
            color: Style.backgroundColor
            width: 400
            height: 400
            AlbumList {}
        }
        Rectangle {
            width: mainWindow.width / 3
            height: mainWindow.height
            Speedometer { id: speedometer }

            OxaniumText {
                anchors.right: speedometer.right
                anchors.bottom: speedometer.bottom
                font.pixelSize: 30
                text: qsTr("To jest mój cluster")
            }
        }
    }
    Loader { id: pageLoder }
    ClusterButton {
        text: "<font color='#f2db8d'>" + qsTr("MAP") + "</font>";
        onClicked: pageLoder.source = "NaviMap.qml";
    }
}
