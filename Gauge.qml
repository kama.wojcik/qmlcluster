import QtQuick 2.14
import QtQuick.Layouts 1.12
import QtGraphicalEffects 1.14
import QtQuick.Shapes 1.14

Rectangle {
    id: frame
    anchors.fill: parent
    color: Style.backgroundColor
    readonly property int offset: 35
    property int currentAngle: 0
    property int currentSpeedValue: 0
    property bool isMirrored: false
    property string unit: "dummy"

    Image {
        id: gauge_frame
        source: "qrc:/assets/gauge-frame.png"
        anchors.centerIn: parent
        scale: 0.5
        mipmap: true
        mirror: isMirrored
    }
    Glow {
        source: gauge_frame
        radius: 4
        samples: 17
        anchors.fill: gauge_frame
        scale: 0.5
        color: "#363d8a"
    }

    Image {
        id: arrow
        source: "qrc:/assets/arrow.png"
        anchors.horizontalCenter: parent.horizontalCenter
        y: parent.height / 2 - sourceSize.height
        transform: Rotation {
            origin.x: frame.x + arrow.width / 2
            origin.y: frame.y + arrow.sourceSize.height
            angle: currentAngle
            Behavior on angle {
                NumberAnimation {
                    duration: 1000
                    easing.type: Easing.OutBack
                }
            }
        }
    }
    Shape {
        anchors.fill: parent
        ShapePath {
            startX: frame.x + frame.width / 2
            startY: frame.y + frame.height / 2 - frame.offset
            strokeColor: "#ffbc00"
            strokeWidth: 1
            fillColor: "#ffbc00"
            capStyle: ShapePath.RoundCap
            strokeStyle: ShapePath.SolidLine
            joinStyle: ShapePath.RoundJoin
            PathArc {
                x: frame.x + frame.width / 2 - 1//frame.x + frame.width / 2 - frame.offset
                y: frame.y + frame.height / 2 - frame.offset//frame.y + frame.height / 2
                radiusX: frame.offset; radiusY: frame.offset
                useLargeArc: true
                direction: PathArc.Clockwise

            }
//            PathArc {
//                x: frame.x + frame.width / 2 - frame.offset
//                y: frame.y + frame.height / 2
//                radiusX: frame.offset; radiusY: frame.offset
//                useLargeArc: false
//                direction: PathArc.Counterclockwise
//            }
        }
    }
    Image {
        id: speedometer_circle
        source: "qrc:circle65.png"
        x: frame.x + frame.width / 2 - frame.offset
        y: frame.y + frame.height / 2 - frame.offset
        scale: 1.2
        OxaniumText {
            id: speed_value
            text: currentSpeedValue
            font.pixelSize: 28
            font.bold: true
            color: "white"
            width: parent.width
            horizontalAlignment: Text.AlignHCenter
        }
        OxaniumText {
            id: speed_unit
            anchors.top: speed_value.bottom
            text: unit
            font.pixelSize: 20
            font.bold: true
            color: "#8a9a9c"
            width: parent.width
            horizontalAlignment: Text.AlignHCenter
        }
    }
    Glow {
        anchors.fill: speedometer_circle
        radius: 20
        samples: 17
        spread: 0.5
        color: "#ffbc00"
        source: speedometer_circle
    }
}
