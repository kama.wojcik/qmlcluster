#include "valueprovider.h"

#include <QTimer>

ValueProvider::ValueProvider(QObject *parent) : QObject(parent), m_speedValue(0)
{
    m_timer = new QTimer(this);
    connect(m_timer, SIGNAL(timeout()), this, SLOT(updateSpeedValue()));
    m_timer->start(1000);
}

ValueProvider::~ValueProvider()
{
    delete m_timer;
}

void ValueProvider::setSpeedValue(int value)
{
    m_speedValue = value;
    emit speedValueChanged();
}


