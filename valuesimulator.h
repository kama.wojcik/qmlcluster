#ifndef VALUESIMULATOR_H
#define VALUESIMULATOR_H

#include "valueprovider.h"

class ValueSimulator : public ValueProvider
{
public:
    ValueSimulator();
    ~ValueSimulator();

    void updateSpeedValue() override;
};

#endif // VALUESIMULATOR_H
