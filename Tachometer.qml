import QtQuick 2.14

Gauge {
    currentAngle: 240
    currentSpeedValue: 2
    isMirrored: false
    unit:  "x1000 rpm"
}
