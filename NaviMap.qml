import QtQuick 2.14
import QtQuick.Controls 2.14
import QtLocation 5.14
import QtPositioning 5.14

Page {
    width: 1600
    height: 480

    Plugin {
        id: mapPlugin
        name: "osm"
    }
    Map {
        anchors.fill: parent
        plugin: mapPlugin
        zoomLevel: 14
        center: QtPositioning.coordinate(51.11, 17.02)

    }
}
