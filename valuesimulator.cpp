#include "valuesimulator.h"

#include <QRandomGenerator>
#include <QTimer>

ValueSimulator::ValueSimulator()
{

}

ValueSimulator::~ValueSimulator()
{

}

void ValueSimulator::updateSpeedValue()
{
    setSpeedValue(QRandomGenerator::global()->bounded(160));
}
