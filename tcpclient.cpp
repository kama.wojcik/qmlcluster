#include "tcpclient.h"

#include <QTcpSocket>
#include <QThread>

TCPClient::TCPClient() : m_tcpSocket(nullptr)
{
    doConnect();
}

void TCPClient::doConnect()
{
    m_tcpSocket = new QTcpSocket(this);

    connect(m_tcpSocket, SIGNAL(connected()), this, SLOT(onConnected()));
    connect(m_tcpSocket, SIGNAL(disconnected()), this, SLOT(onDisconnected()));
    connect(m_tcpSocket, SIGNAL(bytesWritten(qint64)), this, SLOT(onBytesWritten(qint64)));
    connect(m_tcpSocket, SIGNAL(readyRead()), this, SLOT(onReadyRead()));

    qDebug() << "connecting ...";

    m_tcpSocket->connectToHost(VALUE_PROVIDER_HOST, PORT_NUMBER);

    if (!m_tcpSocket->waitForConnected(5000))
    {
        qDebug() << "Error: " << m_tcpSocket->errorString();
    }
}

void TCPClient::updateSpeedValue()
{
    // send request for speed value
    if (m_tcpSocket != nullptr)
    {
        m_tcpSocket->connectToHost(VALUE_PROVIDER_HOST, PORT_NUMBER);
        QByteArray msg = "GET / HTTP/1.1\r\n\r\n\r\n\r\n";
        m_tcpSocket->write(msg);
    }
}

void TCPClient::onConnected()
{
    qDebug() << "connected...";
}

void TCPClient::onDisconnected()
{
    qDebug() << "disconnected...";
}

void TCPClient::onBytesWritten(qint64 bytes)
{
    qDebug() << bytes << " bytes written...";
}

void TCPClient::onReadyRead()
{
    qDebug() << "reading...";

    QByteArray reply = m_tcpSocket->readAll();
    std::string r = QString::fromUtf8(reply).toStdString();
    int speed = 0;
    if (reply != "")
    try
    {
        speed = std::stoi(r.substr(r.find("\r\n\r\n")));
    }
    catch (std::exception e)
    {
        qDebug() << "wrong speed value" << reply;
        speed = 0;
    }

    if (speed < 0 || speed > MAX_SPEED_VALUE)
    {
        qDebug() << "speed value beyond limit: " << speed;
        speed = 0;
        return;
    }
    setSpeedValue(speed);
    //qDebug() << reply;
}
