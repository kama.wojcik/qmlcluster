import SimpleHTTPServer
import SocketServer
from BaseHTTPServer import BaseHTTPRequestHandler
import random

class speedHandler(BaseHTTPRequestHandler):

    #Handler for the GET requests
    def do_GET(self):
        self.send_response(200)
        self.send_header('Content-type','text/html')
        self.end_headers()
        # Send the html message
        speed = random.randint(0, 180)
        self.wfile.write(str(speed))
        return

port = 5555
handler = speedHandler
httpd = SocketServer.TCPServer(("", port), handler)

print 'Serving at port ' + str(port)
httpd.serve_forever()

