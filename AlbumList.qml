import QtQuick 2.14

Rectangle {
    color: Style.backgroundColor
    PathView {
        id: albumList
        height: 400
        width: 400
        model: AlbumModel {}
        delegate: albumDelegate
        Component {
            id: albumDelegate
            Column {
                opacity: PathView.isCurrentItem ? 1 : 0.5
                z: PathView.isCurrentItem ? 1 : 0
                Image {
                    id: cover_img
                    source: cover
                    width: 200
                    height: 200
                    layer.enabled: true
                    layer.effect:  ShaderEffect {
                        property real adjustX: cover_img.adapt ? Math.max(width / height, 1) : 1
                        property real adjustY: cover_img.adapt ? Math.max(1 / (width / height), 1) : 1

                        fragmentShader: "
                        #ifdef GL_ES
                            precision lowp float;
                        #endif // GL_ES
                        varying highp vec2 qt_TexCoord0;
                        uniform highp float qt_Opacity;
                        uniform lowp sampler2D source;
                        uniform lowp float adjustX;
                        uniform lowp float adjustY;

                        void main(void) {
                            lowp float x, y;
                            x = (qt_TexCoord0.x - 0.5) * adjustX;
                            y = (qt_TexCoord0.y - 0.5) * adjustY;
                            float delta = adjustX != 1.0 ? fwidth(y) / 2.0 : fwidth(x) / 2.0;
                            gl_FragColor = texture2D(source, qt_TexCoord0).rgba
                                * step((x * x) + (y * y / 0.8), 0.2)
                                * smoothstep(((x * x) + (y * y / 0.8)) , 0.2 + delta, 0.2)
                                * qt_Opacity;
                        }"
                    }
                }
                OxaniumText { text: name; font.bold: true }
                OxaniumText { text: artist; }
            }
        }
        path: Path {
            startX: 120
            startY: 250
            PathQuad { x: 120; y: 125; controlX: 260; controlY: 175 }
            PathQuad { x: 120; y: 250; controlX: -20; controlY: 175 }
        }
    }
}
