import QtQuick 2.0

ListModel {
    id: albumList

    ListElement {
        name: "100th Window"
        artist: "Massive Atack"
        cover: "qrc:/albums/100th-window.jpg"
    }
    ListElement {
        name: "Best Case Life"
        artist: "Gemini Rising"
        cover: "qrc:/albums/best-case-life.jpg"
    }
    ListElement {
        name: "Discobolus"
        artist: "Bass Astral x Igo"
        cover: "qrc:/albums/discobolus.jpg"
    }
    ListElement {
        name: "Pleasure Centre"
        artist: "Kraak & Smaak"
        cover: "qrc:/albums/pleasure-centre.jpg"
    }
    ListElement {
        name: "18"
        artist: "Moby"
        cover: "qrc:/albums/18.jpg"
    }
}
