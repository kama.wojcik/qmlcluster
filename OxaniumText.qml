import QtQuick 2.0

Text {
    FontLoader { id: oxaniumFont; source: "qrc:/Oxanium-Regular.ttf" }
    font.family: oxaniumFont.name
    font.pixelSize: Style.textSize
    color: Style.textColor
}
