#ifndef SETTINGSMANAGER_H
#define SETTINGSMANAGER_H

#include <QString>

class SettingsManager
{
public:
    SettingsManager();

    QString getSpeedUnit();

private:
    void loadSettings();
    void saveSettings();

private:
    QString m_settingsFile;
    QString m_mode;
    QString m_speedUnit;
    QString m_maxRPM;
};

#endif // SETTINGSMANAGER_H
