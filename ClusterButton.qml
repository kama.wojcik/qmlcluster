import QtQuick 2.14
import QtQuick.Controls 2.14

Button {

    background: Rectangle {
        color: Style.controlsColor
        radius: 2
        width: 50
    }

    highlighted: true
    font.family: oxanium.name

    //contentItem: OxaniumText {}


    FontLoader { id: oxanium; source: "qrc:/Oxanium-Regular.ttf"; }

}
