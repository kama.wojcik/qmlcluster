#ifndef TCPCLIENT_H
#define TCPCLIENT_H

#include "valueprovider.h"

const static int MAX_SPEED_VALUE = 180;

class QTcpSocket;

class TCPClient : public ValueProvider
{
    Q_OBJECT
public:
    TCPClient();
    void doConnect();

public slots:
    void onConnected();
    void onDisconnected();
    void onBytesWritten(qint64 bytes);
    void onReadyRead();

private:
    QTcpSocket * m_tcpSocket;

    void updateSpeedValue() override;
};

#endif // TCPCLIENT_H
