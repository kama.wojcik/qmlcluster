#include "settingsmanager.h"

#include <QSettings>
#include <QUrl>

SettingsManager::SettingsManager()
{
    m_settingsFile = ":/demosettings.ini";
    loadSettings();
}

QString SettingsManager::getSpeedUnit()
{
    return m_speedUnit;
}

void SettingsManager::loadSettings()
{
    QSettings settings(m_settingsFile, QSettings::IniFormat);
    m_mode = settings.value("mode", "normal").toString();
    m_speedUnit = settings.value("speedUnit", "km/h").toString();
    m_maxRPM = settings.value("maxRPM", "8000").toString();
}
