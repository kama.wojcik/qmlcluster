#ifndef VALUEPROVIDER_H
#define VALUEPROVIDER_H

#include <QObject>

class QTimer;

class ValueProvider : public QObject
{
    Q_OBJECT
    Q_PROPERTY(int speedValue MEMBER m_speedValue NOTIFY speedValueChanged)

public:
    explicit ValueProvider(QObject *parent = nullptr);
    virtual ~ValueProvider();

    void virtual setSpeedValue(int value);

signals:
    void speedValueChanged();

protected slots:
    virtual void updateSpeedValue() = 0;

private:
    int m_speedValue;
    QTimer * m_timer;
};

#endif // VALUEPROVIDER_H
