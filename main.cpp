#include <datamodel.h>
#include <valuesimulator.h>
#include <tcpclient.h>

#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>


int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QGuiApplication app(argc, argv);

    DataObject dataObject;
    ValueProvider * valueProvider = new TCPClient();

    QQmlApplicationEngine engine;
    engine.rootContext()->setContextProperty("dataObject", &dataObject);
    engine.rootContext()->setContextProperty("valueProvider", valueProvider);
    const QUrl url(QStringLiteral("qrc:/main.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);
    engine.load(url);

    return app.exec();
}
